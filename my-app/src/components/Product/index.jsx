import { Component } from "react";
import Button from "../Button";
import PropTypes from 'prop-types';

export default class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalContent: {
        modalTitle: "Do you want to add this item?",
        modalText: "Click Ok to add",
        modalActions: [
          <Button
            key={1}
            btnStyle={"btn-success"}
            text={"Ok"}
            onButtonClick={() => {
              this.props.addToCart(this.props.id);
              this.props.closeModal();
              this.setState((state) => ({ itemIsAdded: !state.itemIsAdded }));
            }}
          />,
          <Button
            key={2}
            btnStyle={"btn-outline-danger"}
            text={"Cancel"}
            onButtonClick={() => this.props.closeModal()}
          />,
        ],
        closeButton: false,
      },
    };
  }
  render() {
    const addedItems = JSON.parse(localStorage.getItem("addedItems"));
    const likedItems = JSON.parse(localStorage.getItem("favoriteItems"));
    return (
      <li className="card me-3 mb-3" style={{ maxWidth: 250 + "px" }}>
        <div className="card-body">
          <img src={this.props.imageUrl} alt="" width={"230"} />
          <h5 className="card-title">{this.props.title}</h5>
          <p className="card-text">Vendor code: {this.props.code}</p>
          <p className="card-text">Color: {this.props.color}</p>
          <Button
            btnStyle={"btn-primary"}
            text={"Add to cart"}
            onButtonClick={this.props.handlerButton}
            param={this.state.modalContent}
            isDisabled={
              addedItems.find((itemId) => itemId === this.props.id)
                ? true
                : false
            }
          />
          <img
            src={
              likedItems.find((itemId) => itemId === this.props.id)
                ? "/images/favorite-active.png"
                : "/images/favorite.png"
            }
            width={15}
            height={15}
            alt=""
            onClick={() => {
              this.props.likeItem(this.props.id);
            }}
          />
        </div>
      </li>
    );
  }
}

Product.propTypes = {
  title: PropTypes.string,
  prise: PropTypes.string,
  imageUrl: PropTypes.string,
  id: PropTypes.string,
  color: PropTypes.string,
  code: PropTypes.string,
  likeItem: PropTypes.func,
  handlerButton: PropTypes.func,
  closeModal: PropTypes.func,
  addToCart: PropTypes.func,
}
