import { Component } from "react";
import PropTypes from 'prop-types';
import "./modal.scss";

export default class Modal extends Component {
  constructor(props) {
    super(props);
  }

  closeModal(event) {
    if (!event.target.closest(".card") || event.target.closest(".close-btn")) {
      this.props.onCloseClick();
    }
  }

  render() {
    return (
      <div
        className={`${
          this.props.modalIsOpen ? "modal-wrap--active" : ""
        } modal-wrap`}
        onClick={this.closeModal.bind(this)}
      >
        <div className="card w-50 text-bg-dark mb-3">
          <h5 className="card-header">{this.props.title}</h5>
          {this.props.closeButton && (
            <button className="close-btn">
              <span></span>
              <span></span>
            </button>
          )}
          <div className="card-body">
            <p className="card-text text-center">{this.props.text}</p>
            <div className="main-modal-actions">{this.props.actions}</div>
          </div>
        </div>
      </div>
    );
  }
}

Modal.propTypes={
  actions: PropTypes.array,
  closeButton: PropTypes.bool,
  modalIsOpen: PropTypes.bool,
  onCloseClick: PropTypes.func,
  text: PropTypes.string,
  title: PropTypes.string,
}