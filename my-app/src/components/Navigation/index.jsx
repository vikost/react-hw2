import { Component } from "react";
import PropTypes from "prop-types";

import "./navigation.scss";

export default class Navigation extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <nav className="navbar bg-dark">
        <div className="container-lg">
          <span className="navbar-brand mb-0 h1 text-white">IShop</span>
          <div className="header__selected-info">
            <div className="header__cart">
              <img src="/images/cart.png" alt="" width={"45"} height={"45"} />
              <span className="selected-counter">{this.props.addedItems}</span>
            </div>
            <div className="header__fav">
              <img
                src="/images/favorite-white.png"
                alt=""
                width={"30"}
                height={"30"}
              />
              <span className="selected-counter">{this.props.likedItems}</span>
            </div>
          </div>
        </div>
      </nav>
    );
  }
}

Navigation.propTypes = {
  addedItems: PropTypes.number,
};
