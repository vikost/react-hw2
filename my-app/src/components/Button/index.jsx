import { Component } from "react";
import PropTypes from 'prop-types';

export default class Button extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <button
        className={`btn ${this.props.btnStyle} m-2`}
        onClick={() => {
          this.props.onButtonClick(this.props.param)
        }}
        disabled={this.props.isDisabled}
      >
        {this.props.text}
      </button>
    );
  }
}

Button.propTypes={
  text: PropTypes.string,
  btnStyle: PropTypes.string,
  isDisabled: PropTypes.bool,
  onButtonClick: PropTypes.func,
  param: PropTypes.object,
}