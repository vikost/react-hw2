import { Component } from "react";
import PropTypes from 'prop-types';
import Product from "../Product";

export default class ProductList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
    };
    this.getProducts = this.getProducts.bind(this);
  }
  getProducts() {
    fetch("items.json")
      .then((response) => response.json())
      .then((products) => {
        this.setState({
          products: products,
        });
      });
  }
  componentDidMount() {
    this.getProducts();
  }
  render() {
    return (
      <section>
        <div className="container-lg mt-3">
          <ul className="d-flex flex-wrap">
            {this.state.products.map((product) => {
              return (
                <Product
                  id={product.code}
                  key={product.code}
                  {...product}
                  handlerButton={this.props.modalChange}
                  addToCart={this.props.addItems}
                  likeItem={this.props.likeItem}
                  closeModal={this.props.closeModal}
                />
              );
            })}
          </ul>
        </div>
      </section>
    );
  }
}


ProductList.propTypes = {
  addItems: PropTypes.func,
  closeModal: PropTypes.func,
  likeItem: PropTypes.func,
  modalChange: PropTypes.func,
}