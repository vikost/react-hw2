import { Component } from "react";
import Navigation from "../components/Navigation";
import ProductList from "../components/ProductList";
import Modal from "../components/Modal";

if (!localStorage.getItem("addedItems")) {
  localStorage.setItem("addedItems", "[]");
}
if (!localStorage.getItem("favoriteItems")) {
  localStorage.setItem("favoriteItems", "[]");
}

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalContent: {
        modalTitle: "",
        modalText: "",
        modalActions: [],
        modalCloseBtn: true,
      },
      modalIsOpen: false,
      addedItems: localStorage.getItem("addedItems"),
      favoriteItems: localStorage.getItem("favoriteItems"),
    };
    this.modalChange = this.modalChange.bind(this);
    this.modalChangeState = this.modalChangeState.bind(this);
    this.addItemtoCart = this.addItemtoCart.bind(this);
    this.addItemtoFavorite = this.addItemtoFavorite.bind(this);
  }
  modalChange({ modalTitle, modalText, modalActions, closeButton }) {
    this.setState(() => ({
      modalContent: {
        modalTitle: modalTitle,
        modalText: modalText,
        modalActions: modalActions,
        modalCloseBtn: closeButton,
      },
    }));
    this.modalChangeState();
  }
  modalChangeState() {
    this.setState((state) => ({
      modalIsOpen: !state.modalIsOpen,
    }));
  }
  addItemtoCart(productId) {
    const updatedItems = JSON.parse(localStorage.getItem("addedItems"));
    if (!updatedItems.find((item) => item === productId)) {
      updatedItems.push(productId);
    }
    localStorage.setItem("addedItems", JSON.stringify(updatedItems));
    this.setState({ addedItems: localStorage.getItem("addedItems") });
  }
  addItemtoFavorite(productId) {
    const updatedItems = JSON.parse(localStorage.getItem("favoriteItems"));
    const itemIndex = updatedItems.findIndex((item) => item === productId);
    if (itemIndex === -1) {
      updatedItems.push(productId);
    } else {
      updatedItems.splice(itemIndex, 1);
    }
    localStorage.setItem("favoriteItems", JSON.stringify(updatedItems));
    this.setState({ favoriteItems: localStorage.getItem("favoriteItems") });
  }
  render() {
    return (
      <>
        <Navigation
          addedItems={JSON.parse(this.state.addedItems).length}
          likedItems={JSON.parse(this.state.favoriteItems).length}
        />
        <ProductList
          modalChange={this.modalChange}
          addItems={this.addItemtoCart}
          likeItem={this.addItemtoFavorite}
          closeModal={this.modalChangeState}
        />
        <Modal
          title={this.state.modalContent.modalTitle}
          text={this.state.modalContent.modalText}
          closeButton={this.state.modalContent.modalCloseBtn}
          actions={this.state.modalContent.modalActions}
          modalIsOpen={this.state.modalIsOpen}
          onCloseClick={this.modalChangeState}
        />
      </>
    );
  }
}
